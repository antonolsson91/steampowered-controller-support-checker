## Steampowered Controller Support Checker

Check out what games are couchable!

## Install

Available for Google Chrome on [Chrome Store](https://chrome.google.com/webstore/detail/cbgmpaidfgjjgjcenkeekfnapgiaiael) as an extension.

### Overview

![screenshot](http://puu.sh/4ymzl/33e8374700.jpg)

Powered by jQuery, Adobe Fireworks ([R.I.P](http://blogs.adobe.com/fireworks/2013/05/the-future-of-adobe-fireworks.html)) and [Steam](http://www.steampowered.com/).

## Feedback, Contributions & Questions

You can contact me on <eaglesight@comhem.se> or simply start forking!
To test the extension locally in Chrome, simply open [chrome://extensions/](extensions), click the 'fetch uncompressed extension' button and select the repo folder.