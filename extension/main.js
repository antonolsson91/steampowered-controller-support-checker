(function($) {$.fn.reOrder = function(array) {

    return $(this).children().each( function(){
        for(var i=0; i < array.length; i++){
            $('div[category="' + array[i] + '"]')
                .appendTo( $(this) );
        }
    
    } );

}
})(jQuery);

Storage.prototype.setObject = function (key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function (key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
};

// var steam_account = (jQuery('#account_pulldown').html().split('\'')[0]);
var steam_id = document.URL.split('/')[4] 
var storage_key = "controller_support_" + steam_id;
var gameIndex = localStorage.getObject(storage_key);


function PropagateDOMChanges( gameIndex ){
    for(index = 0; index <= gameIndex.length; index++){

        try{
            var gS = gameIndex[index].support;
            var gID = gameIndex[index].elemid;
            var gC = gameIndex[index].coop;
            
            switch(gS){
                case 2:
                    jQuery('#' + gID + ' .gameListRowLogo .gameLogoHolder_default .gameLogo').addClass('CONTROLLER_SUPPORT');
                    jQuery('#' + gID).attr('category', 'CONTROLLER_SUPPORT');
                break;
                    
                case 1:
                    jQuery('#' + gID + ' .gameListRowLogo .gameLogoHolder_default .gameLogo').addClass('PARTIAL_CONTROLLER_SUPPORT');
                    jQuery('#' + gID).attr('category', 'PARTIAL_CONTROLLER_SUPPORT');
                break;
                
                default:
                    jQuery('#' + gID).attr('category', 'NO_CONTROLLER_SUPPORT');
            }
            
            switch(gC){
                case 1:
                    jQuery('#' + gID + ' .gameListRowLogo .gameLogoHolder_default .gameLogo').addClass('COOP_SUPPORT');
                    jQuery('#' + gID).addClass('COOP_SUPPORT');
                break;

                default:
                    jQuery('#' + gID).addClass('NO_COOP_SUPPORT');
            }

        } catch(e){
            console.log(gID + ": " + e);
            gameIndex[index].support = 0;
            localStorage.setObject(storage_key, gameIndex);
        }

    }

}

function PerformSupportCheck(){

    var totalGames = (jQuery('#games_list_rows > div').length)+1;
    var operationIndex = 0;
    var failedIndexes = 0;
    
    jQuery('.gameListRow').each(function (index) {

        gameIndex[index] = new Object();
        gameIndex[index].elemid = jQuery(this).attr('id');
        gameIndex[index].id = jQuery(this).attr('id').replace("game_", "");
        gameIndex[index].url = jQuery(this).attr('id').replace("game_", "http://store.steampowered.com/app/");
    });

    
    var gets = [];
    jQuery(gameIndex).each(function(index) {
        gets.push(jQuery.get(
        
            gameIndex[index].url, function (data) {
                console.log("Successfully loaded " + gameIndex[index].url);
            })
            .done(function (data) {
                console.log("Done with " + gameIndex[index].url);

                if (data.indexOf('ico_controller.gif') > -1) {
                    gameIndex[index].support = 2;
                } else if (data.indexOf('ico_partial_controller.gif')  > -1) {
                    gameIndex[index].support = 1;
                } else {
                    gameIndex[index].support = 0;
                }
                
                if (data.indexOf('ico_coop.gif') > -1) {
                    gameIndex[index].coop = 1;
                } else {
                    gameIndex[index].coop = 0;
                }
                
            })
            .fail(function (data) {
                console.log("Error loading " + gameIndex[index].url);
                failedIndexes++;
            })
            .always(function (data) {
                console.log("Finished.");
                jQuery('#working_status').html('Operations completed: ' + operationIndex + '/' + totalGames);
                if(failedIndexes > 0) jQuery('#working_status').html( jQuery('#working_status').html() + '<br />Failed operations: ' + failedIndexes);
                operationIndex++;
            })
        
        )
    });

    jQuery.when.apply(jQuery, gets).then(function() {
        
        jQuery('#working_status').html('Operations completed: ' + totalGames + '/' + totalGames);
        
        localStorage.setObject(storage_key, gameIndex);
        console.log('Saved key ' + storage_key + ' with data:'); 
        console.log( gameIndex );

        PropagateDOMChanges( gameIndex );
    });

}


// Legend
legend_html = '<div id="controller_support"><strong>Legend:</strong><div class="CONTROLLER_SUPPORT"><img src="http://cdn3.store.steampowered.com/public/images/ico/ico_controller.gif" alt="" /> Full controller support</div><div class="PARTIAL_CONTROLLER_SUPPORT"><img src="http://cdn3.store.steampowered.com/public/images/ico/ico_partial_controller.gif" alt="" /> Partial controller support</div><button id="sortby-btn">Sort by Controller Support</button><button id="refresh-btn">Refresh</button><div id="working_status"></div></div>';
if( jQuery('#controller_support').html() === undefined ){
    jQuery('body').append(legend_html);
} else {
    jQuery('#controller_support').replaceWith(legend_html);
}

jQuery('#controller_support button#sortby-btn').on( "click", function(e){ 
    e.preventDefault();
    
    jQuery('#games_list_row_container')
        .reOrder([ 'CONTROLLER_SUPPORT', 'PARTIAL_CONTROLLER_SUPPORT', 'NO_CONTROLLER_SUPPORT' ]);
});


jQuery('#controller_support button#refresh-btn').on( "click", function(e){ 
    e.preventDefault();

    PerformSupportCheck();
});


if(gameIndex && gameIndex[0].support !== undefined){
    PropagateDOMChanges( gameIndex );
} else {
    var gameIndex = [];
    PerformSupportCheck();
}


